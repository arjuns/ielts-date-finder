﻿using System;
using System.Text;

namespace Kaagati.Platform
{
    public static class ExceptionHelpers
    {
        public static void ThrowIfArgumentIsNull(object args, string name)
        {
            if (args == null)
            {
                throw new ArgumentNullException("name");
            }
        }

        public static string FlattenException(this Exception exception)
        {
            var stringBuilder = new StringBuilder();

            while (exception != null)
            {
                stringBuilder.AppendLine(exception.Message);
                stringBuilder.AppendLine(exception.StackTrace);
                exception = exception.InnerException;
            }

            return stringBuilder.ToString();
        }
    }
}