﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Formapp.Scheduler;

namespace Formapp
{
    public class JobRunner
    {
        private readonly TimeSpan interval;
        readonly List<IJob> jobs = new List<IJob>();
        private readonly object sync = new object();
        public event EventHandler SearchStarted = delegate { };

        public JobRunner(TimeSpan interval)
        {
            this.interval = interval;

        }

        public TimeSpan Inverval
        {
            get { return interval; }
        }

        public void AddJob(IJob job)
        {
            lock (sync)
            {
                this.jobs.Add(job);
            }

        }


        private bool started;

        public bool IsRunning
        {
            get { return started; }
        }
        private CancellationTokenSource cts = new CancellationTokenSource();
        private Task task;

        public void Start()
        {
            if (started)
                throw new InvalidOperationException("Runner already started");
            cts = new CancellationTokenSource();
            this.task = StartInternal();
        }
        public async Task RequestStop()
        {
            if (this.IsRunning)
            {
                cts.Cancel();
                await this.task;
                this.started = false;
            }
        }

        private Task StartInternal()
        {

            return Task.Run(() =>
              {
                  this.started = true;
                  while (!cts.IsCancellationRequested)
                  {
                      OnStarted();
                      List<IJob> cache = null;
                      lock (sync)
                      {
                          cache = new List<IJob>(this.jobs);
                      }
                      Task.Run(() =>
                      {
                          foreach (var job in cache)
                          {
                              if (cts.IsCancellationRequested)
                              {
                                  break;
                              }
                              job.Execute();
                          }
                      });

                      if (cts.Token.WaitHandle.WaitOne(this.interval))
                      {
                          break;
                      }


                  }
              }, cts.Token);
        }

        private void OnStarted()
        {
            this.SearchStarted(this, EventArgs.Empty);
        }

     

        public void ClearJob()
        {
            lock (sync)
            {
                this.jobs.Clear();
            }
        }
    }
}