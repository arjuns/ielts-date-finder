using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Formapp
{
    public static class VenueFormatter
    {
        public static string FormatVenues(IList<ModuleAvailability> venues)
        {

            StringBuilder format = new StringBuilder();
            foreach (var group in venues.OrderBy(x => x.Location)
                .ThenBy(x => x.Date)
                .GroupBy(x => x.Location))
            {
                format.AppendLine(String.Format("{0} [{1}]", group.Key, group.Count()));
                format.AppendLine("=====================");
                group.ToList().ForEach(x=>format.AppendLine(x.FormatString()));
                format.AppendLine("");

            }
            return format.ToString();

        }
    }
}