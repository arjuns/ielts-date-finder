using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Formapp
{
    public class AppConfiguration
    {
        [JsonProperty("mail")]
        public MailSettings MailSettings { get; set; }

        [JsonProperty("receivers")]
        public List<String> EmailReceivers { get; set; } 
    }
    public static class ConfigurationStore
    {
        private const string SETTING_FILE="settings.config";

        public static AppConfiguration ReadSettings()
        {
            if (File.Exists(SETTING_FILE))
            {
                try
                {
                    var content = File.ReadAllText(SETTING_FILE);
                    return JsonConvert.DeserializeObject<AppConfiguration>(content);
                }
                catch
                {
                    MainApp.Logger.Error("Error reading settings. Please provide new settings in settings tab");
                }
            }
            MainApp.Logger.Info("Initializing empty settings. Fill correct value from settings tag");
            return new AppConfiguration();
        }

        public static void WriteSettings(AppConfiguration config)
        {
            var content = JsonConvert.SerializeObject(config,Formatting.Indented);
            File.WriteAllText(SETTING_FILE, content);
        }
    }
}