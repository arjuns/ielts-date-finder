﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CsQuery.Engine.PseudoClassSelectors;
using Formapp.Scheduler;
using Kaagati.Platform;

namespace Formapp
{
    public partial class MainApp : Form
    {
        readonly JobRunner runner = new JobRunner(TimeSpan.FromMinutes(1));
        private readonly System.Timers.Timer nextRoundTimer = new System.Timers.Timer();
        private DateSubscriber resultSubscriber;
        public MainApp()
        {
            InitializeComponent();
            this.Load += WindowLoaded;
            Countries = new BindingList<CountryItem>();
            emailReceivers = new BindingList<string>();
            nextRoundTimer.Interval = 1000;
            SetupTimer();
        }

        private void SetupTimer()
        {
            var currentTime = runner.Inverval;
            nextRoundTimer.Elapsed +=
                (a, b) =>
                {
                    currentTime = currentTime.Add(TimeSpan.FromSeconds(-1));
                    this.toolStrip_nextTime.Text = String.Format("Serching after {0}", currentTime.ToString("g"));
                };

            runner.SearchStarted += (a, b) =>
            {
                nextRoundTimer.Stop();
                Logger.Debug("Search started");
                currentTime = runner.Inverval;
                nextRoundTimer.Enabled = true;
                nextRoundTimer.Start();

            };

        }

        public static ILogger Logger { get; private set; }

        public readonly BindingList<CountryItem> Countries;
        public readonly BindingList<String> emailReceivers;

        private void WindowLoaded(object sender, EventArgs e)
        {

            Logger = new BeautifulLogger(this.fastColoredTextBox1, SynchronizationContext.Current);

            this.Text = "IELTS date finder";
            this.comboBoxModule.SelectedIndex = 0;
            this.comboBoxFilter.SelectedIndex = 0;

            this.Countries.Add(new CountryItem("Nepal", "nepal"));
            this.Countries.Add(new CountryItem("India", "india"));
            this.Countries.Add(new CountryItem("Bangladesh", "bangladesh"));
            this.comboBoxCountry.DataSource = this.Countries;
            this.listBoxEmailReceivers.DataSource = this.emailReceivers;

            this.comboBoxFilter.SelectedIndexChanged += FilterSelectionChanged;

            #region commented

            //StringBuilder sb=new StringBuilder();

            //CQ query = CQ.Create(File.ReadAllText("C:\\data.txt"));
            //foreach (var node in query["option"])
            //{
            //    var innerText = node.InnerText.Trim();
            //    innerText = innerText.Replace(" ", "_").
            //        Replace("-", "_").
            //        Replace(",","_").Replace("(","")
            //        .Replace(")","").
            //        Replace("'","").Replace("__","_");


            //    var value = node["value"];
            //    sb.AppendFormat(" public static Guid {0}=new Guid(\"{1}\");", innerText, value);
            //    sb.AppendLine("");

            //}

            #endregion

            resultSubscriber = new DateSubscriber(new NepalDateFinderJob(), Logger);
            this.runner.AddJob(resultSubscriber.Job);

            LoadSettingsControl();
            Logger.Info(String.Format("Finder interval 5 minutes"));
        }

        private void LoadSettingsControl()
        {
            var config = ConfigurationStore.ReadSettings();
            var mailSettings = config.MailSettings;


            if (mailSettings != null)
            {
                this.textBox_emailsender.Text = mailSettings.SenderEmail;
                this.textBox_emailsenderPassword.Text = mailSettings.SenderPassword;
                this.textBox_smtpPort.Text = mailSettings.Port.ToString();
                this.textBox_smtpServer.Text = mailSettings.SmtpServer;
                this.checkBox_useSSL.Checked = mailSettings.UseSsl;
            }
            if (config.EmailReceivers != null && config.EmailReceivers.Count > 0)
            {
                foreach (var receiver in config.EmailReceivers)
                {
                    this.emailReceivers.Add(receiver);
                }
            }
        }

        private List<ModuleAvailability> lastResult;

        public List<ModuleAvailability> GetVenues(Finder.Module module, string country)
        {

            Finder finder = new Finder(country);
            lastResult = finder.GetAllVenues(module);
            return lastResult;

        }

        public List<ModuleAvailability> FilterResult(bool showAll)
        {
            List<ModuleAvailability> filtered = new List<ModuleAvailability>();
            if (lastResult != null)
            {
                if (showAll)
                    return lastResult;
                filtered.AddRange(lastResult.Where(x => x.Availability == DateAvailability.Available));
            }
            return filtered;
        }


        private async void FindClicked(object sender, EventArgs e)
        {

            lastResult = null;
            if (this.comboBoxCountry.SelectedItem == null)
            {
                this.textBox1.Text = "Invalid country";
                return;

            }
            string country = ((CountryItem)this.comboBoxCountry.SelectedItem).Key;

            Finder.Module module = Finder.Module.Academic;
            this.textBox1.Text = @"Finding all available dates...";
            if (this.comboBoxModule.SelectedItem.Equals("General"))
            {
                module = Finder.Module.General;
            }
            var data = await Task.Run(() =>
            {
                var venues = GetVenues(module, country);
                return VenueFormatter.FormatVenues(venues);
            });

            this.textBox1.Text = data;
        }


        private void FilterSelectionChanged(object sender, EventArgs e)
        {

        }

        private void button_SaveSmtp_Click(object sender, EventArgs e)
        {
            var config = ConfigurationStore.ReadSettings();
            try
            {
                MailSettings settings = new MailSettings();
                settings.Port = Int32.Parse(this.textBox_smtpPort.Text.Trim());
                settings.UseSsl = this.checkBox_useSSL.Checked;
                settings.SenderEmail = this.textBox_emailsender.Text.Trim();
                settings.SenderPassword = this.textBox_emailsenderPassword.Text.Trim();
                settings.SmtpServer = this.textBox_smtpServer.Text.Trim();
                config.MailSettings = settings;

                config.EmailReceivers = new List<string>(this.emailReceivers);
                ConfigurationStore.WriteSettings(config);
                MessageBox.Show(@"Setting Saved", @"Mail Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (FormatException)
            {
                MessageBox.Show(@"Invalid port specified, it should be number", @"Mail Settings", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show(@"Settings can not be saved! an error orrured.", @"Mail Settings", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }




        }

        private async void button_startJob_Click(object sender, EventArgs e)
        {
            if (!this.runner.IsRunning)
            {

                if (ValidateJobRunner())
                {
                    runner.Start();
                    SetStartInfo();
                }


            }
            else
            {
                Logger.Info("Stopping runner.");
                await this.runner.RequestStop();
                SetStopInfo();
            }
        }

        private void SetStopInfo()
        {
            Logger.Warn("Runner is Stopped.");
            this.button_startJob.Text = "Start";
            this.toolstripStatusLabel.Text = "Stopped";
            this.toolstripStatusLabel.Image = Formapp.Properties.Resources.paused;
            this.toolStrip_nextTime.Text = "☺";
            nextRoundTimer.Stop();
        }

        private void SetStartInfo()
        {
            this.button_startJob.Text = "Stop";
            this.toolstripStatusLabel.Text = "Running...";
            this.toolstripStatusLabel.Image = Formapp.Properties.Resources.progress;
        }

        private bool ValidateJobRunner()
        {
            return true;
        }

        private void linkLabel_LogClear_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.fastColoredTextBox1.Clear();
        }

        private void button_addEmailReceiver_Click(object sender, EventArgs e)
        {
            try
            {
                MailAddress address = new MailAddress(this.textBox_emailReceiver.Text.Trim());
                this.emailReceivers.Add(address.Address);
                this.textBox_emailReceiver.Clear();
            }
            catch
            {


            }
        }

        private void linkLabel_ClearSmtpSetting_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.textBox_emailsender.Clear();
            this.textBox_emailsenderPassword.Clear();
            this.textBox_smtpPort.Clear();
            this.textBox_smtpServer.Clear();
            this.checkBox_useSSL.Checked = false;

        }
    }
}
