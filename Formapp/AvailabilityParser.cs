using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CsQuery;

namespace Formapp
{
    public class AvailabilityParser
    {
        public List<ModuleAvailability> GetVenues(string document)
        {
            List<ModuleAvailability> all = new List<ModuleAvailability>();
            var cq = CQ.Create(document);

            var venues = cq["div[class='pnlHeaderBoxLeft'] > p > span[class='searchCriteriaBoxCriteriaValue']"];

            var outer = cq["div[class='pnlBodyDetailRowBoxOuter']"];


            for (int i = 0; i < venues.Count(); i++)
            {
                var location = ParseLocation(venues[i].InnerText.Trim());

                var inter = CQ.CreateFragment(outer[i].OuterHTML);
                var rows = new CQ(inter["div[class='pnlBodyDetailRowBox']"]);
                foreach (var row in rows)
                {
                    ModuleAvailability availability = new ModuleAvailability();
                    var fragment = CQ.CreateFragment(row.OuterHTML);
                    var doms = fragment["div[class='pnlBodyDetailRowBoxLeft']"].ToList();
                    availability.Date = ParseDate(doms[0].InnerText.Trim());
                    availability.Module = doms[1].InnerText.Trim();
                    availability.Fee = doms[2].InnerText.Trim();
                    availability.Location = location;
                    availability.Availability = ParseAvailability(doms[3].InnerText.Trim());

                    if (availability.Availability == DateAvailability.Available)
                    {
                        availability.AvailablePercentage = ParseAvailabilityPercentage(doms[3].InnerText.Trim());
                    }
                    all.Add(availability);

                }



            }



            return all;
        }

        private string ParseLocation(string location)
        {
            var many = location.IndexOf(',');
            if (many > 0)
                return location.Substring(0, many);
            return location;
        }

        private DateTime ParseDate(string trim)
        {
            try
            {
                return DateTime.ParseExact(trim, BritisCouncilBrowser.AVAILIBILITY_DATE_FORMAT, CultureInfo.InvariantCulture);
            }
            catch
            {
            }
            return DateTime.MinValue;
        }

        private int ParseAvailabilityPercentage(string ava)
        {
            var match = Regex.Match(ava, "\\d+");
            if (match.Success)
                return Int32.Parse(match.Value);
            return 0;
        }

        private DateAvailability ParseAvailability(string str)
        {
            if (str.ToLower().Contains("available"))
                return DateAvailability.Available;
            if (str.ToLower().Contains("closed"))
                return DateAvailability.Closed;
            if (str.ToLower().Contains("full"))
                return DateAvailability.Full;
            return DateAvailability.Full;
        }
    }
}