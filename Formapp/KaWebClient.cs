﻿using System;
using System.Net;

namespace Formapp
{
    [System.ComponentModel.DesignerCategory(@"Code")]
    public class KaWebClient : WebClient
    {

        public KaWebClient()
            : this(new CookieContainer())
        { }
        public KaWebClient(CookieContainer c)
        {

            this.CookieContainer = c;
        }
        public CookieContainer CookieContainer { get; private set; }



        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = (HttpWebRequest)base.GetWebRequest(address);
            request.ProtocolVersion = HttpVersion.Version11;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.CookieContainer = this.CookieContainer;
            return request;

        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            var response = (HttpWebResponse)base.GetWebResponse(request);
            this.CookieContainer.Add((response).Cookies);
            return response;
        }
    }
}