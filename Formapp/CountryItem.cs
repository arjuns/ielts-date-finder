﻿namespace Formapp
{
    public class CountryItem
    {
        public CountryItem(string name, string value)
        {
            this.Name = name;
            this.Key = value;
        }

        public string Name { get; set; }
        public string Key { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}