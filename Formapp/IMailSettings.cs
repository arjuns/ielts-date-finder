using System;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace Formapp
{
    public interface IMailSettings
    {
        [JsonProperty("email")]
        String SenderEmail { get; }
        [JsonProperty("password")]
        [JsonConverter(typeof(PasswordConverter))]
        String SenderPassword { get; }
        [JsonProperty("server")]
        String SmtpServer { get; }
        [JsonProperty("port")]
        int Port { get; }
        [JsonProperty("use_ssl")]
        bool UseSsl { get; }
    }

    public class MailSettings : IMailSettings
    {
        public string SenderEmail { get; set; }
        public string SenderPassword { get; set; }
        public string SmtpServer { get; set; }
        public int Port { get; set; }
        public bool UseSsl { get; set; }
    }

    public sealed class PasswordConverter : JsonConverter
    {
        private const string PASS_KEY = "arjuns@1235634534#$$#~_(*&";

        
      
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(Crypto.EncryptStringAES(value.ToString(), PASS_KEY));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return Crypto.DecryptStringAES(reader.Value.ToString(), PASS_KEY);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(String);
        }
    }
}