using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CsQuery;

namespace Formapp
{
    public class Finder
    {
        private readonly string country;


        public Finder(string country)
        {
            this.country = country;

        }

        public enum Module
        {
            Academic = 1,
            General = 2,
            All = 3
        }
        private const string CHECK_AVAILABILITY = "https://ielts.britishcouncil.org/CheckAvailability.aspx";




        public List<ModuleAvailability> GetAllVenues(Module module)
        {

            List<ModuleAvailability> allVenues = new List<ModuleAvailability>();
            var builder = new PostDataBuilder();
            var browser = new BritisCouncilBrowser(this.country);

            var downloadString = browser.BrowseHomePage();
            var cq = CQ.Create(downloadString);
            List<DateTime> examdate = ParseExamDate(cq);

            if (examdate.Count == 0)
                return new List<ModuleAvailability>();

            AvailabilityParser parser = new AvailabilityParser();
            

            foreach (var dateTime in examdate)
            {
                builder.SetEventTarget(cq["input[id='__EVENTTARGET']"].Val());
                builder.SetEventValidation(cq["input[id='__EVENTVALIDATION']"].Val());
                builder.SetViewState(cq["input[id='__VIEWSTATE']"].Val());
                builder.Add("ctl00$ContentPlaceHolder1$ddlTownCityVenue:0"); //all venue
                builder.Add("ctl00$ContentPlaceHolder1$imgbSearch.x:50"); //random image click point x
                builder.Add("ctl00$ContentPlaceHolder1$imgbSearch.y:12"); //random image click point y, browser sends click point if image is used as a submit button
                builder.SetModule(module);
                builder.SetSearchDate(dateTime);


                var method = builder.ToString();
                browser.Post(browser.GetHomePageUrl(), method);
                // At the best only one redirect should happen here 
                //but the problem is when it redirects  it requires a valid Referer header to be set but webclient does not allow us to do that 
                //so we are emulating redirect ourselves with submitting a get request to the CHECK_AVAILABILITY event

                var availibilityData = browser.Get(CHECK_AVAILABILITY);


                var result = parser.GetVenues(availibilityData);
                allVenues.AddRange(result);
            }

            #region Commented, Search page
            ////Collect first round of data

            ////as the first date data is already collected now try with reamaining date
            //foreach (var date in examdate.Skip(1))
            //{
            //    cq = new CQ(availibilityData);
            //    builder.Clear();
            //    builder.SetEventTarget(cq["input[id='__EVENTTARGET']"].Val());
            //    builder.SetEventValidation(cq["input[id='__EVENTVALIDATION']"].Val());
            //    builder.SetEventArgument(cq["input[id='__EVENTARGUMENT']"].Val());
            //    builder.SetViewState(cq["input[id='__VIEWSTATE']"].Val());
            //    builder.SetSearchDate(date);
            //    builder.Add("ctl00$ContentPlaceHolder1$ddlTownCityVenue:Any");
            //    builder.Add("ctl00$ContentPlaceHolder1$imgbSearch.x:50"); //random image click point x
            //    builder.Add("ctl00$ContentPlaceHolder1$imgbSearch.y:12"); //random image click point y, browser sends click point if image is used as a submit button
            //    builder.SetModule(module);
            //    availibilityData = browser.Post(CHECK_AVAILABILITY, builder.ToString());
            //    var result = parser.GetVenues(availibilityData);
            //    allVenues.AddRange(result);

            //}
            #endregion

            return MergeVenue(allVenues);
        }

        private List<ModuleAvailability> MergeVenue(IEnumerable<ModuleAvailability> allVenues)
        {
            return allVenues.Distinct(ModuleAvailability.ModuleAvailabilityComparer).ToList();
        }

        private List<DateTime> ParseExamDate(CQ cq)
        {
            List<DateTime> result = new List<DateTime>();

            var dateHolder = cq["select[id='ctl00_ContentPlaceHolder1_ddlDateMonthYear']"].First();
            var fragment = CQ.CreateFragment(dateHolder);
            var one = fragment["option"];
            foreach (var node in one)
            {
                try
                {
                    var dateTime = DateTime.ParseExact(node["value"], BritisCouncilBrowser.SEARCH_DATE_FORMAT, CultureInfo.InvariantCulture);
                    result.Add(dateTime);
                }
                catch { }
            }
            return result;
        }
    }

    
}