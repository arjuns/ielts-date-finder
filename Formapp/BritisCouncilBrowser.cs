using System;
using System.Collections;
using System.Net;
using System.Reflection;
using System.Text;

namespace Formapp
{
    public class BritisCouncilBrowser
    {
        public const string SEARCH_DATE_FORMAT = "dd/MM/yyyy";
        public const string AVAILIBILITY_DATE_FORMAT = "dd MMMM yyyy";
        private readonly string countryName;
        private const string HOME_PAGE = "https://ielts.britishcouncil.org/{0}";
        public const string CHECK_AVAILABILITY
            = "https://ielts.britishcouncil.org/CheckAvailability.aspx";
        private CookieContainer cookieContainer;
        public BritisCouncilBrowser(string countryName)
        {
            this.countryName = countryName;
            cookieContainer = new CookieContainer();
        }

        public String BrowseHomePage()
        {
            return Get(GetHomePageUrl());
        }

        public string GetHomePageUrl()
        {
            return String.Format(HOME_PAGE, this.countryName.ToLower());
        }

        public void ClearCookie()
        {
            cookieContainer = new CookieContainer();
        }

        public static CookieCollection GetAllCookies(CookieContainer cookieJar)
        {
            CookieCollection cookieCollection = new CookieCollection();

            Hashtable table = (Hashtable)cookieJar.GetType().InvokeMember("m_domainTable",
                                                                            BindingFlags.NonPublic |
                                                                            BindingFlags.GetField |
                                                                            BindingFlags.Instance,
                                                                            null,
                                                                            cookieJar,
                                                                            new object[] { });

            foreach (var tableKey in table.Keys)
            {
                String strTableKey = (string)tableKey;

                if (strTableKey[0] == '.')
                {
                    strTableKey = strTableKey.Substring(1);
                }

                SortedList list = (SortedList)table[tableKey].GetType().InvokeMember("m_list",
                                                                            BindingFlags.NonPublic |
                                                                            BindingFlags.GetField |
                                                                            BindingFlags.Instance,
                                                                            null,
                                                                            table[tableKey],
                                                                            new object[] { });

                foreach (var listKey in list.Keys)
                {
                    String url = "https://" + strTableKey + (string)listKey;
                    cookieCollection.Add(cookieJar.GetCookies(new Uri(url)));
                }
            }

            return cookieCollection;
        }

        public string ReadCookie()
        {
            var cookieCollection = GetAllCookies(this.cookieContainer);
            StringBuilder sb = new StringBuilder();
            foreach (Cookie cokie in cookieCollection)
            {
                sb.AppendFormat("{0}= {1}", cokie.Name, cokie.Value);
                sb.AppendLine("");
            }
            return sb.ToString();
        }


        public string Get(string url)
        {

            KaWebClient client = new KaWebClient(cookieContainer);

            client.Headers[HttpRequestHeader.Accept] =
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";

            //this is very required
            client.Headers[HttpRequestHeader.Referer] = GetReferrer();
            client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip,deflate,sdch";
            client.Headers[HttpRequestHeader.AcceptLanguage] = "en-GB,en-US;q=0.8,en;q=0.6";
            client.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";


            return client.DownloadString(url);
        }

        private string GetReferrer()
        {
            return String.Format("https://ielts.britishcouncil.org/{0}", this.countryName.ToLower());
        }

        public string Post(string url, string data)
        {
           
            KaWebClient client = new KaWebClient(this.cookieContainer);

            client.Headers.Add("Origin", "https://ielts.britishcouncil.org");
            client.Headers[HttpRequestHeader.Accept] =
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip,deflate,sdch";
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            client.Headers[HttpRequestHeader.AcceptLanguage] = "en-GB,en-US;q=0.8,en;q=0.6";
            client.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
            client.Headers[HttpRequestHeader.Host] = "ielts.britishcouncil.org";
            client.Headers[HttpRequestHeader.Referer] = GetReferrer();
            return client.UploadString(url, data);



        }
    }
}