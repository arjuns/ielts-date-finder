﻿using System;

namespace Formapp.Scheduler
{
    public interface IJob<T>:IJob
    {
        event EventHandler<T> PostResult;
    }

    public interface IJob
    {
        void Execute();
    }
    





}