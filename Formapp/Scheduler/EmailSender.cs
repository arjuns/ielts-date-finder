﻿using System.Collections.Generic;
using System.Net;
using System.Net.Mail;

namespace Formapp.Scheduler
{

    /*
            * Server Name	    SMTP Address	    Port	    SSL
            *  Yahoo!	    smtp.mail.yahoo.com	    587	        Yes
               GMail	    smtp.gmail.com	        587	        Yes
               Hotmail	    smtp.live.com	        587	        Yes
            */
    public class EmailSender
    {
        
        private readonly IMailSettings settings;


        public EmailSender(IMailSettings settings)
        {
            
            this.settings = settings;
        }

        public void Send(IEnumerable<string> recepients, string subjectLine,string body)
        {
            MailMessage msg = new MailMessage();
            msg.Sender = new MailAddress(settings.SenderEmail);
            msg.From=new MailAddress(settings.SenderEmail);
            msg.Subject = subjectLine ?? "Available Ielts Date(s)";
            foreach (var recepient in recepients)
            {
                msg.To.Add(new MailAddress(recepient));
            }
            msg.Body = body;
            
            using (SmtpClient smtp = new SmtpClient(settings.SmtpServer, settings.Port))
            {
                smtp.Credentials = new NetworkCredential(settings.SenderEmail, settings.SenderPassword);
                smtp.EnableSsl = true;
                smtp.Send(msg);
                MainApp.Logger.Info("Mail Sent");
            }

        }
    }

}