using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Itenso.TimePeriod;
using Kaagati.Platform;

namespace Formapp.Scheduler
{
    public class DateSubscriber
    {
        private readonly DateTime startDate;
        private readonly DateTime endDate;
        private readonly IJob<List<ModuleAvailability>> job;
        private readonly ILogger logger;

        public IJob Job
        {
            get { return job; }
        }

        public DateSubscriber(IJob<List<ModuleAvailability>> job, ILogger logger)
        {
            this.job = job;
            this.logger = logger ?? new NullLogger();
            job.PostResult += SubscribeResult;
            var now = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            startDate = now;
            endDate = new DateTime(DateTime.Now.Year, 11, 1);
            
            endDate = endDate.AddMonths(1).AddDays(-1); // end of december

        }

        private void SubscribeResult(object ori, List<ModuleAvailability> result)
        {
            var filtered = ResultFilter(result);

            var config = ConfigurationStore.ReadSettings();
            if (filtered.Count > 0)
            {
                logger.Info(String.Format("{0} matching date found", filtered.Count));
            }
            else
            {
                string subject = String.Format("No matching dates found from {0} to {1}",
                           startDate.ToString("MMMM yyyy"),
                           endDate.ToString("MMMM yyyy"));
                logger.Debug(subject);
            }

            if (filtered.Count > 0)
            {
                //offload email sending 
                Task.Run(() =>
                {
                    if (config.EmailReceivers != null && config.EmailReceivers.Count > 0)
                    {
                        var body = VenueFormatter.FormatVenues(filtered);
                        EmailSender sender = new EmailSender(config.MailSettings);

                        string subject = String.Format("All available date from {0} to end of {1}",
                            startDate.ToString("MMMM yyyy"),
                            endDate.ToString("MMMM yyyy"));

                        sender.Send(config.EmailReceivers, subject, body);
                    }
                    else
                    {
                        logger.Warn("Email can not be sent as no receiver is found");
                    }
                });

            }
        }

        private List<ModuleAvailability> ResultFilter(IEnumerable<ModuleAvailability> venues)
        {
            return venues.Where(x =>
                (x.Availability == DateAvailability.Available)
                && IsDateWithinRange(x.Date)).ToList();

        }

        public bool IsDateWithinRange(DateTime date)
        {
            TimePeriodCollection periods = new TimePeriodCollection();
            periods.Add(new TimeRange(startDate, endDate));
            return periods.HasIntersectionPeriods(date);
        }

    }
}