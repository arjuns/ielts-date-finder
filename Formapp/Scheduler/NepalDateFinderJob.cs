﻿using System;
using System.Collections.Generic;

namespace Formapp.Scheduler
{
    public class NepalDateFinderJob : IJob<List<ModuleAvailability>>
    {

        public void Execute()
        {
            Finder finder = new Finder("nepal");
            var result = finder.GetAllVenues(Finder.Module.Academic);
            this.PostResult(this, result);
        }
        public event EventHandler<List<ModuleAvailability>> PostResult = delegate { };
    }
}