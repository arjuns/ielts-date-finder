﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Formapp
{
    public class ModuleAvailability
    {
        public DateTime Date { get; set; }
        public String Module { get; set; }
        public String Fee { get; set; }
        public DateAvailability Availability { get; set; }
        public int AvailablePercentage { get; set; }
        public String Location { get; set; }

        private sealed class ModuleAvailabilityEqualityComparer : IEqualityComparer<ModuleAvailability>
        {
            public bool Equals(ModuleAvailability x, ModuleAvailability y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.Date.Equals(y.Date) && string.Equals(x.Module, y.Module) && string.Equals(x.Fee, y.Fee) && x.Availability == y.Availability && x.AvailablePercentage == y.AvailablePercentage && string.Equals(x.Location, y.Location);
            }

            public int GetHashCode(ModuleAvailability obj)
            {
                unchecked
                {
                    var hashCode = obj.Date.GetHashCode();
                    hashCode = (hashCode * 397) ^ (obj.Module != null ? obj.Module.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (obj.Fee != null ? obj.Fee.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (int)obj.Availability;
                    hashCode = (hashCode * 397) ^ obj.AvailablePercentage;
                    hashCode = (hashCode * 397) ^ (obj.Location != null ? obj.Location.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }

        private static readonly IEqualityComparer<ModuleAvailability> ModuleAvailabilityComparerInstance = new ModuleAvailabilityEqualityComparer();

        public static IEqualityComparer<ModuleAvailability> ModuleAvailabilityComparer
        {
            get { return ModuleAvailabilityComparerInstance; }
        }
    }

    public static class Helpers
    {
        public static string FormatString(this ModuleAvailability record)
        {
            if (record.AvailablePercentage > 0)
            {
                return String.Format("{0}  {1}  {2}  {3} ({4}%) ", record.Date.ToString("D"), record.Module,
                    record.Fee,
                    record.Availability, record.AvailablePercentage);
            }
            return String.Format("{0}  {1}  {2}  {3} ", record.Date.ToString("D"), record.Module,
                record.Fee,
                record.Availability);
        }
    }


    public enum DateAvailability
    {
        Closed,
        Full,
        Available
    }
}