using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;

namespace Formapp
{
    public class PostDataBuilder
    {
        private readonly Dictionary<string, string> items;

        public PostDataBuilder()
        {
            items = new Dictionary<string, string>();
        }
        public void Set(string name, string value)
        {
            items[name] = value;
        }

        public void Add(String keyvalue)
        {
            var values = keyvalue.Split(':');
            Set(values[0], values[1]);
        }

        public void Clear()
        {
            this.items.Clear();

        }
        public override string ToString()
        {

            List<string> local = new List<string>();
            foreach (var item in items)
            {
                local.Add(String.Format("{0}={1}", HttpUtility.UrlEncode(item.Key), HttpUtility.UrlEncode(item.Value)));
            }
            return String.Join("&", local);

        }

        public void SetModule(Finder.Module module)
        {
            Set("ctl00$ContentPlaceHolder1$ddlModule", ((int)module).ToString(CultureInfo.InvariantCulture));
        }

        public void SetEventTarget(string value)
        {
            Set("__EVENTTARGET",value);
        }
        public void SetEventValidation(string value)
        {
            Set("__EVENTVALIDATION", value);
        }
        
        public void SetViewState(string value)
        {
            Set("__VIEWSTATE", value);
        }


        public void SetSearchDate(DateTime first)
        {
            var searchDate = first.ToString("dd/MM/yyyy");
            Set("ctl00$ContentPlaceHolder1$ddlDateMonthYear", searchDate);
        }

        public void SetEventArgument(string val)
        {
            Set("__EVENTARGUMENT",val);
        }
    }
}