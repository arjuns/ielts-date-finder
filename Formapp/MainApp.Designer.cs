﻿namespace Formapp
{
    partial class MainApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.comboBoxCountry = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxFilter = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxModule = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listBoxEmailReceivers = new System.Windows.Forms.ListBox();
            this.button_addEmailReceiver = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.textBox_emailReceiver = new System.Windows.Forms.TextBox();
            this.button_SaveSmtp = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkLabel_ClearSmtpSetting = new System.Windows.Forms.LinkLabel();
            this.textBox_emailsenderPassword = new System.Windows.Forms.TextBox();
            this.checkBox_useSSL = new System.Windows.Forms.CheckBox();
            this.textBox_smtpPort = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_emailsender = new System.Windows.Forms.TextBox();
            this.textBox_smtpServer = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.fastColoredTextBox1 = new FastColoredTextBoxNS.FastColoredTextBox();
            this.linkLabel_LogClear = new System.Windows.Forms.LinkLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_startJob = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolstripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip_nextTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(10, 96);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(543, 194);
            this.textBox1.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(222, 295);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Find Dates";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.FindClicked);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(588, 500);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.comboBoxCountry);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.comboBoxFilter);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.comboBoxModule);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(580, 474);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Availibility";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBoxCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCountry.FormattingEnabled = true;
            this.comboBoxCountry.Location = new System.Drawing.Point(111, 20);
            this.comboBoxCountry.Name = "comboBoxCountry";
            this.comboBoxCountry.Size = new System.Drawing.Size(121, 21);
            this.comboBoxCountry.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(372, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Show";
            // 
            // comboBoxFilter
            // 
            this.comboBoxFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFilter.FormattingEnabled = true;
            this.comboBoxFilter.Items.AddRange(new object[] {
            "All",
            "Only Available"});
            this.comboBoxFilter.Location = new System.Drawing.Point(430, 69);
            this.comboBoxFilter.Name = "comboBoxFilter";
            this.comboBoxFilter.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFilter.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Country";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(364, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Module";
            // 
            // comboBoxModule
            // 
            this.comboBoxModule.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxModule.FormattingEnabled = true;
            this.comboBoxModule.Items.AddRange(new object[] {
            "Academic",
            "General"});
            this.comboBoxModule.Location = new System.Drawing.Point(430, 25);
            this.comboBoxModule.Name = "comboBoxModule";
            this.comboBoxModule.Size = new System.Drawing.Size(121, 21);
            this.comboBoxModule.TabIndex = 3;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.button_SaveSmtp);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(580, 474);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Settings";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBoxEmailReceivers);
            this.groupBox3.Controls.Add(this.button_addEmailReceiver);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.textBox_emailReceiver);
            this.groupBox3.Location = new System.Drawing.Point(8, 206);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(530, 200);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Email Receivers";
            // 
            // listBoxEmailReceivers
            // 
            this.listBoxEmailReceivers.FormattingEnabled = true;
            this.listBoxEmailReceivers.Location = new System.Drawing.Point(21, 54);
            this.listBoxEmailReceivers.Name = "listBoxEmailReceivers";
            this.listBoxEmailReceivers.Size = new System.Drawing.Size(482, 121);
            this.listBoxEmailReceivers.TabIndex = 7;
            // 
            // button_addEmailReceiver
            // 
            this.button_addEmailReceiver.Location = new System.Drawing.Point(428, 25);
            this.button_addEmailReceiver.Name = "button_addEmailReceiver";
            this.button_addEmailReceiver.Size = new System.Drawing.Size(75, 23);
            this.button_addEmailReceiver.TabIndex = 8;
            this.button_addEmailReceiver.Text = "Add Receiver";
            this.button_addEmailReceiver.UseVisualStyleBackColor = true;
            this.button_addEmailReceiver.Click += new System.EventHandler(this.button_addEmailReceiver_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(97, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(117, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Receiver email address";
            // 
            // textBox_emailReceiver
            // 
            this.textBox_emailReceiver.Location = new System.Drawing.Point(220, 27);
            this.textBox_emailReceiver.Name = "textBox_emailReceiver";
            this.textBox_emailReceiver.Size = new System.Drawing.Size(191, 20);
            this.textBox_emailReceiver.TabIndex = 1;
            // 
            // button_SaveSmtp
            // 
            this.button_SaveSmtp.Location = new System.Drawing.Point(213, 432);
            this.button_SaveSmtp.Name = "button_SaveSmtp";
            this.button_SaveSmtp.Size = new System.Drawing.Size(111, 23);
            this.button_SaveSmtp.TabIndex = 6;
            this.button_SaveSmtp.Text = "Save";
            this.button_SaveSmtp.UseVisualStyleBackColor = true;
            this.button_SaveSmtp.Click += new System.EventHandler(this.button_SaveSmtp_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkLabel_ClearSmtpSetting);
            this.groupBox1.Controls.Add(this.textBox_emailsenderPassword);
            this.groupBox1.Controls.Add(this.checkBox_useSSL);
            this.groupBox1.Controls.Add(this.textBox_smtpPort);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox_emailsender);
            this.groupBox1.Controls.Add(this.textBox_smtpServer);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(8, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(531, 181);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mail";
            // 
            // linkLabel_ClearSmtpSetting
            // 
            this.linkLabel_ClearSmtpSetting.AutoSize = true;
            this.linkLabel_ClearSmtpSetting.Location = new System.Drawing.Point(453, 16);
            this.linkLabel_ClearSmtpSetting.Name = "linkLabel_ClearSmtpSetting";
            this.linkLabel_ClearSmtpSetting.Size = new System.Drawing.Size(72, 13);
            this.linkLabel_ClearSmtpSetting.TabIndex = 6;
            this.linkLabel_ClearSmtpSetting.TabStop = true;
            this.linkLabel_ClearSmtpSetting.Text = "Clear Settings";
            this.linkLabel_ClearSmtpSetting.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_ClearSmtpSetting_LinkClicked);
            // 
            // textBox_emailsenderPassword
            // 
            this.textBox_emailsenderPassword.Location = new System.Drawing.Point(161, 47);
            this.textBox_emailsenderPassword.Name = "textBox_emailsenderPassword";
            this.textBox_emailsenderPassword.PasswordChar = '*';
            this.textBox_emailsenderPassword.Size = new System.Drawing.Size(191, 20);
            this.textBox_emailsenderPassword.TabIndex = 2;
            // 
            // checkBox_useSSL
            // 
            this.checkBox_useSSL.AutoSize = true;
            this.checkBox_useSSL.Location = new System.Drawing.Point(161, 125);
            this.checkBox_useSSL.Name = "checkBox_useSSL";
            this.checkBox_useSSL.Size = new System.Drawing.Size(68, 17);
            this.checkBox_useSSL.TabIndex = 5;
            this.checkBox_useSSL.Text = "Use SSL";
            this.checkBox_useSSL.UseVisualStyleBackColor = true;
            // 
            // textBox_smtpPort
            // 
            this.textBox_smtpPort.Location = new System.Drawing.Point(161, 99);
            this.textBox_smtpPort.Name = "textBox_smtpPort";
            this.textBox_smtpPort.Size = new System.Drawing.Size(191, 20);
            this.textBox_smtpPort.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Email Sender Address";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(45, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Smtp Port";
            // 
            // textBox_emailsender
            // 
            this.textBox_emailsender.Location = new System.Drawing.Point(161, 21);
            this.textBox_emailsender.Name = "textBox_emailsender";
            this.textBox_emailsender.Size = new System.Drawing.Size(191, 20);
            this.textBox_emailsender.TabIndex = 1;
            // 
            // textBox_smtpServer
            // 
            this.textBox_smtpServer.Location = new System.Drawing.Point(161, 73);
            this.textBox_smtpServer.Name = "textBox_smtpServer";
            this.textBox_smtpServer.Size = new System.Drawing.Size(191, 20);
            this.textBox_smtpServer.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Password";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 73);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Smtp Server";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Controls.Add(this.linkLabel_LogClear);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(580, 474);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Monitoring Service";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.fastColoredTextBox1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox4.Location = new System.Drawing.Point(0, 262);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox4.Size = new System.Drawing.Size(580, 212);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Log";
            // 
            // fastColoredTextBox1
            // 
            this.fastColoredTextBox1.AutoScrollMinSize = new System.Drawing.Size(2, 14);
            this.fastColoredTextBox1.BackBrush = null;
            this.fastColoredTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.fastColoredTextBox1.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.fastColoredTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fastColoredTextBox1.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.fastColoredTextBox1.IsReplaceMode = false;
            this.fastColoredTextBox1.Location = new System.Drawing.Point(10, 23);
            this.fastColoredTextBox1.Name = "fastColoredTextBox1";
            this.fastColoredTextBox1.Paddings = new System.Windows.Forms.Padding(0);
            this.fastColoredTextBox1.ReadOnly = true;
            this.fastColoredTextBox1.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.fastColoredTextBox1.ShowLineNumbers = false;
            this.fastColoredTextBox1.Size = new System.Drawing.Size(560, 179);
            this.fastColoredTextBox1.TabIndex = 1;
            // 
            // linkLabel_LogClear
            // 
            this.linkLabel_LogClear.AutoSize = true;
            this.linkLabel_LogClear.Location = new System.Drawing.Point(518, 246);
            this.linkLabel_LogClear.Name = "linkLabel_LogClear";
            this.linkLabel_LogClear.Size = new System.Drawing.Size(52, 13);
            this.linkLabel_LogClear.TabIndex = 3;
            this.linkLabel_LogClear.TabStop = true;
            this.linkLabel_LogClear.Text = "Clear Log";
            this.linkLabel_LogClear.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LogClear_LinkClicked);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_startJob);
            this.groupBox2.Location = new System.Drawing.Point(44, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(404, 101);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Control";
            // 
            // button_startJob
            // 
            this.button_startJob.Location = new System.Drawing.Point(141, 37);
            this.button_startJob.Name = "button_startJob";
            this.button_startJob.Size = new System.Drawing.Size(75, 23);
            this.button_startJob.TabIndex = 0;
            this.button_startJob.Text = "Start";
            this.button_startJob.UseVisualStyleBackColor = true;
            this.button_startJob.Click += new System.EventHandler(this.button_startJob_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolstripStatusLabel,
            this.toolStripStatusLabel1,
            this.toolStrip_nextTime});
            this.statusStrip1.Location = new System.Drawing.Point(0, 500);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(588, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolstripStatusLabel
            // 
            this.toolstripStatusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolstripStatusLabel.Image = global::Formapp.Properties.Resources.paused;
            this.toolstripStatusLabel.Name = "toolstripStatusLabel";
            this.toolstripStatusLabel.Size = new System.Drawing.Size(16, 17);
            this.toolstripStatusLabel.Text = "Stopped";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(538, 17);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // toolStrip_nextTime
            // 
            this.toolStrip_nextTime.Name = "toolStrip_nextTime";
            this.toolStrip_nextTime.Size = new System.Drawing.Size(19, 17);
            this.toolStrip_nextTime.Text = "☺";
            // 
            // MainApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(588, 522);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "MainApp";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fastColoredTextBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxModule;
        private System.Windows.Forms.ComboBox comboBoxFilter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCountry;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBox_emailsender;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button_SaveSmtp;
        private System.Windows.Forms.TextBox textBox_emailsenderPassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_smtpPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_smtpServer;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox_useSSL;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button_startJob;
        private System.Windows.Forms.GroupBox groupBox2;
        private FastColoredTextBoxNS.FastColoredTextBox fastColoredTextBox1;
        private System.Windows.Forms.LinkLabel linkLabel_LogClear;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolstripStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ListBox listBoxEmailReceivers;
        private System.Windows.Forms.Button button_addEmailReceiver;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_emailReceiver;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.LinkLabel linkLabel_ClearSmtpSetting;
        private System.Windows.Forms.ToolStripStatusLabel toolStrip_nextTime;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}

