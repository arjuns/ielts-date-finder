﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FastColoredTextBoxNS;
using Kaagati.Platform;

namespace Formapp
{
    public class BeautifulLogger :
        ILogger
    {
        private readonly FastColoredTextBox fctb;
        private readonly SynchronizationContext context;
        private readonly TextStyle infoStyle;
        private readonly TextStyle exceptionStyle;
        private readonly TextStyle errorStyle;
        private readonly TextStyle warnStyle;
        private readonly TextStyle debugStyle;

        public BeautifulLogger(FastColoredTextBox fctb, SynchronizationContext context)
        {
            this.fctb = fctb;
            this.context = context;
            this.infoStyle = new TextStyle(new SolidBrush(Color.FromArgb(16, 148, 8)), (Brush)null, FontStyle.Regular);
            this.exceptionStyle = new TextStyle(new SolidBrush(Color.FromArgb(232, 76, 76)), (Brush)null, FontStyle.Regular);
            this.errorStyle = new TextStyle(new SolidBrush(Color.FromArgb(232, 76, 76)), (Brush)null, FontStyle.Regular);
            this.warnStyle = new TextStyle(Brushes.SaddleBrown, (Brush)null, FontStyle.Regular);
            this.debugStyle = new TextStyle(Brushes.DarkGray, (Brush)null, FontStyle.Italic);
        }

        public void Info(string message)
        {
            RunOnUIThread(() =>
            {
                Log(message + Environment.NewLine, infoStyle);
                this.fctb.SelectionStart = this.fctb.Text.Length;
            });
        }

        public void Debug(string message)
        {
            RunOnUIThread(() =>
            {
                Log(message + Environment.NewLine, debugStyle);
                this.fctb.SelectionStart = this.fctb.Text.Length;
            });
        }

        public void Error(string message)
        {
            RunOnUIThread(() =>
            {
                Log(message + Environment.NewLine, errorStyle);
                this.fctb.SelectionStart = this.fctb.Text.Length;
            });
        }

        public void Exception(Exception ex, string msg = "")
        {
            RunOnUIThread(() =>
            {
                Log(ex.FlattenException() + Environment.NewLine, exceptionStyle);
                this.fctb.SelectionStart = this.fctb.Text.Length;
            });
        }

        public void Warn(string message)
        {
            RunOnUIThread(() =>
            {
                Log(message + Environment.NewLine, warnStyle);
                this.fctb.SelectionStart = this.fctb.Text.Length;
            });
        }

        private void Log(string text, Style style)
        {
            text = String.Format("[{0}] {1}", DateTime.Now.ToString("hh:mm:ss"), text);
            this.fctb.BeginUpdate();
            this.fctb.Selection.BeginUpdate();
            //if (this.fctb.Text.Length > 30000)
            //    this.fctb.Clear();
            Range range = this.fctb.Selection.Clone();
            this.fctb.AppendText(text, style);
            if (!range.IsEmpty || range.Start.iLine < this.fctb.LinesCount - 2)
            {
                this.fctb.Selection.Start = range.Start;
                this.fctb.Selection.End = range.End;
            }
            else
                this.fctb.GoEnd();
            this.fctb.Selection.EndUpdate();
            this.fctb.EndUpdate();
        }

        public void RunOnUIThread(Action act)
        {
            context.Send((a) => act(), null);
        }
    }
}
