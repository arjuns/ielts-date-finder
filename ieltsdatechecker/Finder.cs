using CsQuery;

namespace ieltsdatechecker
{
    public class Finder
    {
        public enum Module
        {
            Academic = 1,
            General = 2,
            All = 3
        }
        private const string CHECK_AVAILABILITY = "https://ielts.britishcouncil.org/CheckAvailability.aspx";
        private const string HOME_PAGE = "https://ielts.britishcouncil.org/nepal";
        public void Find(Module module)
        {


            PostDataBuilder builder = new PostDataBuilder();
            BritisCouncilBrowser browser = new BritisCouncilBrowser();
            var downloadString = browser.Get(HOME_PAGE);

            var cq = CQ.Create(downloadString);
            builder.SetEventTarget(cq["input[id='__EVENTTARGET']"].Val());
            builder.SetEventValidation(cq["input[id='__EVENTVALIDATION']"].Val());
            builder.SetViewState(cq["input[id='__VIEWSTATE']"].Val());
            builder.Add("ctl00$ContentPlaceHolder1$ddlDateMonthYear:01/08/2014");
            builder.Add("ctl00$ContentPlaceHolder1$ddlTownCityVenue:0"); //all venue
            builder.Add("ctl00$ContentPlaceHolder1$imgbSearch.x:50"); //random image click point x
            builder.Add("ctl00$ContentPlaceHolder1$imgbSearch.y:12"); //random image click point y, browser sends click point if image is used as a submit button



            var method = builder.ToString();
            var data = browser.Post(HOME_PAGE, method);

            // At the best only one redirect should happen here 
            //but the problem is when it redirects  it requires a valid Referer header to be set but webclient does not allow us to do that 
            //so we are emulating redirect ourselves with submitting a post request to the CHECK_AVAILABILITY event
            cq = new CQ(browser.Get(CHECK_AVAILABILITY));

            builder.Clear();

            builder.SetEventTarget(cq["input[id='__EVENTTARGET']"].Val());
            builder.SetEventValidation(cq["input[id='__EVENTVALIDATION']"].Val());
            builder.SetViewState(cq["input[id='__VIEWSTATE']"].Val());

            builder.Add("ctl00$ContentPlaceHolder1$ddlDateMonthYear:01/08/2014");
            builder.Add("ctl00$ContentPlaceHolder1$ddlTownCityVenue:Any");
            builder.Add("ctl00$ContentPlaceHolder1$imgbSearch.x:50"); //random image click point x
            builder.Add("ctl00$ContentPlaceHolder1$imgbSearch.y:12"); //random image click point y, browser sends click point if image is used as a submit button
            builder.SetModule(module);
            var post = browser.Post(CHECK_AVAILABILITY, builder.ToString());
            
        }


    }
}