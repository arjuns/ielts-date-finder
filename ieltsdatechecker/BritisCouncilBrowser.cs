using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;

namespace ieltsdatechecker
{
    public class BritisCouncilBrowser
    {
        public const string CHECK_AVAILABILITY
            = "https://ielts.britishcouncil.org/CheckAvailability.aspx";
        private CookieContainer cookieContainer;
        public BritisCouncilBrowser()
        {
            cookieContainer = new CookieContainer();
        }

        public void ClearCookie()
        {
            cookieContainer = new CookieContainer();
        }

        public static CookieCollection GetAllCookies(CookieContainer cookieJar)
        {
            CookieCollection cookieCollection = new CookieCollection();

            Hashtable table = (Hashtable)cookieJar.GetType().InvokeMember("m_domainTable",
                                                                            BindingFlags.NonPublic |
                                                                            BindingFlags.GetField |
                                                                            BindingFlags.Instance,
                                                                            null,
                                                                            cookieJar,
                                                                            new object[] { });

            foreach (var tableKey in table.Keys)
            {
                String strTableKey = (string)tableKey;

                if (strTableKey[0] == '.')
                {
                    strTableKey = strTableKey.Substring(1);
                }

                SortedList list = (SortedList)table[tableKey].GetType().InvokeMember("m_list",
                                                                            BindingFlags.NonPublic |
                                                                            BindingFlags.GetField |
                                                                            BindingFlags.Instance,
                                                                            null,
                                                                            table[tableKey],
                                                                            new object[] { });

                foreach (var listKey in list.Keys)
                {
                    String url = "https://" + strTableKey + (string)listKey;
                    cookieCollection.Add(cookieJar.GetCookies(new Uri(url)));
                }
            }

            return cookieCollection;
        }

        public string ReadCookie()
        {
            var cookieCollection = GetAllCookies(this.cookieContainer);
            StringBuilder sb = new StringBuilder();
            foreach (Cookie cokie in cookieCollection)
            {
                sb.AppendFormat("{0}= {1}", cokie.Name, cokie.Value);
                sb.AppendLine("");
            }
            return sb.ToString();
        }


        public string Get(string url)
        {

            KaWebClient client = new KaWebClient(cookieContainer);

            client.Headers[HttpRequestHeader.Accept] =
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";

            //this is very required
            client.Headers[HttpRequestHeader.Referer] = "https://ielts.britishcouncil.org/nepal";
            client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip,deflate,sdch";
            client.Headers[HttpRequestHeader.AcceptLanguage] = "en-GB,en-US;q=0.8,en;q=0.6";
            client.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
            

            return client.DownloadString(url);
        }

        public string Post(string url, string data)
        {
            #region commented
            //var readCookie = ReadCookie();
            //byte[] bytes = Encoding.UTF8.GetBytes(data);
            //var request = (HttpWebRequest)HttpWebRequest.Create(url);
            //request.Method = "POST";
            //request.ContentLength = bytes.Length;
            //request.ContentType = "application/x-www-form-urlencoded";
            //request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            //request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
            //request.Host = "ielts.britishcouncil.org";
            //request.KeepAlive = true;
            //request.Headers.Add("Origin", "https://ielts.britishcouncil.org");
            //request.Referer = "https://ielts.britishcouncil.org/nepal";
            //request.Expect = null;
            //request.AllowAutoRedirect = false;
            //request.Headers.Add("Cache-Control", "max-age=0");
            //request.Headers[HttpRequestHeader.AcceptLanguage] = "en-GB,en-US;q=0.8,en;q=0.6";


            //request.CookieContainer = this.cookieContainer;


            //using (var requestStream = request.GetRequestStream())
            //{
            //    requestStream.Write(bytes, 0, bytes.Length);
            //}
            //var response = (HttpWebResponse)request.GetResponse();
            //using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            //{
            //    var location = response.Headers["Location"];
            //    var responseUri = response.ResponseUri;
            //    var httpStatusCode = response.StatusCode;
            //    foreach (Cookie cookie in response.Cookies)
            //    {
            //        this.cookieContainer.Add(cookie);
            //    }
            //    KaWebClient client=new KaWebClient(this.cookieContainer);
            //    var val = client.DownloadString(CHECK_AVAILABILITY);


            //    var read = reader.ReadToEnd();
            //    return read;

            //}
            #endregion

            KaWebClient client = new KaWebClient(this.cookieContainer);
            
            client.Headers.Add("Origin", "https://ielts.britishcouncil.org");
            client.Headers[HttpRequestHeader.Accept] =
                "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8";
            client.Headers[HttpRequestHeader.AcceptEncoding] = "gzip,deflate,sdch";
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
            client.Headers[HttpRequestHeader.AcceptLanguage] = "en-GB,en-US;q=0.8,en;q=0.6";
            client.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36";
            client.Headers[HttpRequestHeader.Host] = "ielts.britishcouncil.org";
            client.Headers[HttpRequestHeader.Referer] = "https://ielts.britishcouncil.org/nepal";
            return client.UploadString(url, data);



        }
    }
}